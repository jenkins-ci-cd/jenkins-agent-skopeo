FROM openshift/jenkins-slave-base-centos7

MAINTAINER Christian Brugger <christian.brugger@doubleslash.de>

RUN yum install -y skopeo && \ 
    yum clean all